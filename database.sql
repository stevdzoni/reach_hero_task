-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for osx10.10 (x86_64)
--
-- Host: localhost    Database: reach_hero_db
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `reach_hero_db`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `reach_hero_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `reach_hero_db`;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(48) NOT NULL,
  `lastName` varchar(48) NOT NULL,
  `email` varchar(256) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Edward','Norton','norton@test.com','2020-02-14 03:05:37','2020-02-14 03:05:37'),(2,'Kate','Beckinsale','beckinsale@test.com','2020-02-14 03:05:54','2020-02-14 03:05:54'),(3,'Scarlett','Johansson','johansson@test.com','2020-02-14 03:06:13','2020-02-14 03:06:13'),(4,'Michael','Fassbender','fassbender@test.com','2020-02-14 03:06:36','2020-02-14 03:06:36'),(5,'Tom','Hardy','hardy@test.com','2020-02-14 03:06:51','2020-02-14 03:06:51'),(6,'Anne','Hathaway','hathaway@test.com','2020-02-14 03:07:24','2020-02-14 03:07:24'),(7,'Sandra','Bullock','bullock@test.com','2020-02-14 03:07:36','2020-02-14 03:07:36'),(8,'Julia','Roberts','roberts@test.com','2020-02-14 03:07:55','2020-02-14 03:07:55'),(9,'Matt','Damon','damon@test.com','2020-02-14 03:08:10','2020-02-14 03:08:10'),(10,'Robert','DeNiro','deniro@test.com','2020-02-14 03:08:27','2020-02-14 03:08:27'),(11,'Rosmund','Pike','pike@test.com','2020-02-14 03:08:46','2020-02-14 03:08:46'),(12,'Natalie','Portman','portman@test.com','2020-02-14 03:09:08','2020-02-14 03:09:08'),(13,'Benicio','Del Toro','deltoro@test.com','2020-02-14 03:09:26','2020-02-14 03:09:26'),(14,'Sean','Penn','penn@test.com','2020-02-14 03:09:41','2020-02-14 03:09:41'),(15,'Demi','Moore','moore@test.com','2020-02-14 03:09:53','2020-02-14 03:10:28');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2y$12$cjdMVzFJUHZvODJmYytad.d9lSTJM5xN53Dz1L1vZEWScxT0mCKIy');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-14  3:24:00
