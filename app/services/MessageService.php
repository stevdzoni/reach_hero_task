<?php

use Phalcon\Flash\FlashInterface;

class MessageService
{
//    private Message $message;
//    private FlashInterface $flash;

//    public function __construct(Message $message, FlashInterface $flash)
//    {
//        $this->message = $message;
//        $this->flash = $flash;
//    }

    public function displayMessage(Message $message, FlashInterface $flash)
    {
        switch ($message->type) {
            case 'error':
                $flash->error($message->content);
                break;
            case 'success':
                $flash->success($message->content);
                break;
            case 'warning':
                $flash->warning($message->content);
                break;
            default:
                $flash->notice($message->content);
        }
    }
}