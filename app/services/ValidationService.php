<?php

use Phalcon\Validation\ValidationInterface;
use Phalcon\Http\RequestInterface;
use Phalcon\Flash\FlashInterface;

class ValidationService
{
    private MessageService $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function isValid (
        ValidationInterface $validation,
        RequestInterface $request,
        FlashInterface $flash
    ) {
        if (! in_array($request->getMethod(), ['POST', 'PUT'])) {
            $message = new Message(
                Message::ERROR,
                Message::HTTP_METHOD_INVALID,
                [$request->getMethod()]
            );

            $this->messageService->displayMessage($message, $flash);
            return false;
        }

        $messages = $validation->validate($request->getPost() ?: $request->getPut());

        if (count($messages) > 0) {
            $message = $message = new Message(Message::ERROR, null, [], $messages[0]);
            $this->messageService->displayMessage($message, $flash);
            return false;
        }

        return true;
    }
}