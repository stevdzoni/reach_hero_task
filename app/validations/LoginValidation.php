<?php

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation;

class LoginValidation extends Validation
{
    public function initialize()
    {
        $this->add('username', new PresenceOf(['message' => 'The username is required.']));
        $this->add('password', new PresenceOf(['message' => 'The password is required.']));

//        $this->add('csrf', new Identical([
//            'accepted'   => $this->security->checkToken(),
//            'message' => 'CSRF forgery detected!'
//        ]));
    }
}