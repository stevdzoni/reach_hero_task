<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\StringLength;

class StoreCustomerValidation extends Validation
{
    public function initialize()
    {
        $this->setTextInputValidators();
        $this->setEmailValidators();
    }

    public function setTextInputValidators()
    {
        $this->add('firstName', new PresenceOf(['message' => 'The first name is required.',]));
        $this->add('lastName', new PresenceOf(['message' => 'The last name is required.',]));

        $this->add('firstName', new StringLength([
            'max' => 40,
            'min' => 2,
            'messageMaximum' => 'You cannot use more than 40 letters for the first name',
            'messageMinimum' => 'You must use at least 2 letters for the first name.'
        ]));
        $this->add('lastName', new StringLength([
            'max' => 40,
            'min' => 2,
            'messageMaximum' => 'You cannot use more than 40 letters for the last name.',
            'messageMinimum' => 'You must use at least 2 letters for the last name.'
        ]));
    }

    public function setEmailValidators()
    {
        $this->add('email', new PresenceOf(['message' => 'The email address is not set.']));
        $this->add('email', new EmailValidator(['message' => 'The email is not valid.',]));
    }
}