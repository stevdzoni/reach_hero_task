<?php

use Phalcon\Filter\FilterFactory;
use Phalcon\Http\Request;
use Phalcon\Filter\FilterInterface;

class CustomerSanitizer
{
    private FilterInterface $locator;

    public function __construct(FilterFactory $filterFactory)
    {
        $this->locator = $filterFactory->newInstance();
    }

    public function sanitize(Request $request)
    {
        $sanitizedData = [];

        if ($request->getPost('id')) {
            $sanitizedData['id'] = $this->locator->sanitize($request->getPost('id'), 'absint');
        }

        $sanitizedData['firstName'] = $this->locator->sanitize(
            $request->getPost('firstName'),
            ['trim', 'string', 'straptags']
        );

        $sanitizedData['lastName'] = $this->locator->sanitize(
            $request->getPost('lastName'),
            ['trim', 'string', 'straptags']
        );

        $sanitizedData['email'] = $this->locator->sanitize($request->getPost('email'), 'email');

        if ($request->getPost('createdAt')) {
            $sanitizedData['createdAt'] = $this->locator->sanitize(
                $request->getPost('createdAt'),
                ['trim', 'string', 'straptags']
            );
        }

        if ($request->getPost('updatedAt')) {
            $sanitizedData['updatedAt'] = $this->locator->sanitize(
                $request->getPost('updatedAt'),
                ['trim', 'string', 'straptags']
            );
        }

        return $sanitizedData;
    }
}