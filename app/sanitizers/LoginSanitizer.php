<?php

use Phalcon\Filter\FilterFactory;
use Phalcon\Filter\FilterInterface;
use Phalcon\Http\Request;

class LoginSanitizer
{
    private FilterInterface $locator;

    public function __construct(FilterFactory $filterFactory)
    {
        $this->locator = $filterFactory->newInstance();
    }

    public function sanitize(Request $request)
    {
        $sanitizedData = [];

        $sanitizedData['username'] = $this->locator->sanitize(
            $request->getPost('username'),
            ['trim', 'string', 'straptags']
        );

        $sanitizedData['password'] = $this->locator->sanitize(
            $request->getPost('password'),
            ['trim', 'string', 'straptags']
        );

        return $sanitizedData;
    }
}