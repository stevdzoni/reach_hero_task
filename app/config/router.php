<?php

use Phalcon\Mvc\Router\Route;

$router = $di->getRouter();

$router->attach(new Route('/', 'Login::index'));
$router->attach(new Route('/login', 'Login::login'));


//exit(print_r($router));
$router->handle($_SERVER['REQUEST_URI']);
