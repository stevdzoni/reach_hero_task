<?php

use Phalcon\Config;

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'reach_hero_db',
        'charset'     => 'utf8',
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'formsDir'       => APP_PATH . '/forms/',
        'modelsDir'      => APP_PATH . '/models/',
        'routesDir'      => APP_PATH . '/routes/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'sanitizersDir'  => APP_PATH . '/sanitizers/',
        'validationsDir' => APP_PATH . '/validations/',
        'viewsDir'       => APP_PATH . '/views/',
        'servicesDir'    => APP_PATH . '/services/',
        'commonDir'      => APP_PATH . '/common/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'baseUri'        => '/',
    ]
]);
