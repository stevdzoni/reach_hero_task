<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class LogoutController extends Controller
{
    public function indexAction()
    {

    }

    public function logoutAction()
    {
        $this->session->remove('username');
        return (new Response())->redirect('/logout');
    }
}