<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

class WelcomeController extends Controller
{
    public function indexAction()
    {
        if (! $this->session->has('username')) {
            $message = new Message(
                Message::NOTICE,
                Message::USER_SESSION_EXPIRED
            );
            $this->session->remove('username');
            $this->messageService->displayMessage($message, $this->flashSession);
            return (new Response())->redirect('/logout');
        }

        $this->view->setVar('username', $this->session->get('username'));
    }
}