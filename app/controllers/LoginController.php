<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter\FilterFactory;
use Phalcon\Http\Response;

class LoginController extends Controller
{
    public function indexAction()
    {
        if ($this->session->has('username')) {
            return (new Response())->redirect('/customers/list');
        }
        $this->view->setVar('form', new LoginForm());
    }

    public function loginAction()
    {
        if (! $this->validationService->isValid(new LoginValidation(), $this->request, $this->flashSession)) {
            return (new Response())->redirect('/');
        }

        $loginSanitizer = new LoginSanitizer(new FilterFactory());
        $sanitizedUserData = $loginSanitizer->sanitize($this->request);

        $username = $sanitizedUserData['username'] ?: '';
        $password = $sanitizedUserData['password'] ?: '';

        $user = Users::findFirst("username ='" . $username . "'");

        if (!$user) {
            $message = new Message(
                Message::ERROR,
                Message::USERNAME_NOT_CORRECT
            );
            $this->messageService->displayMessage($message, $this->flashSession);
            return (new Response())->redirect('/');
        }

        $isUserVerified = $this->checkHashPassword($password, $user->password);

        if ($isUserVerified === false) {
            $message = new Message(
                Message::ERROR,
                Message::PASSWORD_NOT_CORRECT
            );
            $this->messageService->displayMessage($message, $this->flashSession);
            $this->security->hash(rand());
            return (new Response())->redirect('/');
        }

        $this->session->set('username', $sanitizedUserData['username']);
        return (new Response())->redirect('/welcome');
    }

    public function checkHashPassword($password, $passwordHash)
    {
        return $this->security->checkHash($password, $passwordHash);
    }
}