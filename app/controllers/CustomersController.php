<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Filter\FilterFactory;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class CustomersController extends Controller
{
    /**
     * display the list of customers with pagination
     */
    public function listAction()
    {
        $currentPage = $this->request->getQuery('page', 'int', 1);

        $paginator   = new PaginatorModel(
            [
                'model'  => Customers::class,
                'limit' => 5,
                'page'  => $currentPage,
            ]
        );

        $page = $paginator->paginate();
        $this->view->setVar('page', $page);
    }

    /**
     * display a form for a new customer creation
     */
    public function newAction()
    {
        $this->view->setVar('form', new CustomersForm());
    }

    /**
     * create a new customer in the database
     */
    public function createAction()
    {
        if (! $this->validationService->isValid(new StoreCustomerValidation(), $this->request, $this->flashSession)) {
            return (new Response())->redirect('/customers/new/');
        }

        $customerSanitizer = new CustomerSanitizer(new FilterFactory());
        $sanitizedRequest = $customerSanitizer->sanitize($this->request);

        $customer = new Customers();
        $customer->setId(null);
        $customer->setFirstName($sanitizedRequest['firstName']);
        $customer->setLastName($sanitizedRequest['lastName']);
        $customer->setEmail($sanitizedRequest['email']);
        $customer->setCreatedAt(date('Y-m-d H:i:s'));
        $customer->setUpdatedAt(date('Y-m-d H:i:s'));

        try {
            if ($customer->save() !== false) {
                $message = new Message(
                    Message::SUCCESS,
                    Message::RESOURCE_SUCCESSFULLY_CREATED,
                    [Customers::RESOURCE_NAME, $customer->getLastName()]
                );
                $this->messageService->displayMessage($message, $this->flashSession);
                (new CustomersForm())->clear();
            }
        } catch (\Exception $e) {
            // todo: log some data about the error and handle exceptions
            $message = new Message(
                Message::ERROR,
                Message::RESOURCE_NOT_SUCCESSFULLY_CREATED,
                [Customers::RESOURCE_NAME]
            );
            $this->messageService->displayMessage($message, $this->flashSession);
            return (new Response())->redirect('/customers/new/');
        }

        return (new Response())->redirect('/customers/list');
    }

    /**
     * display a form for editing existing customer
     */
    public function editAction($id)
    {
        $customer = Customers::findFirst($id);
        $this->view->setVar('form', new CustomersForm($customer, ['edit' => true]));
    }

    /**
     * update existing customer in the database
     */
    public function saveAction()
    {
        if (! $this->validationService->isValid(new StoreCustomerValidation(), $this->request, $this->flashSession)) {
            return (new Response())->redirect('/customers/edit/' . $this->request->getPost('id'));
        }

        $customerSanitizer = new CustomerSanitizer(new FilterFactory());
        $sanitizedRequest = $customerSanitizer->sanitize($this->request);

        $customer = new Customers();
        $customer->setId($sanitizedRequest['id']);
        $customer->setFirstName($sanitizedRequest['firstName']);
        $customer->setLastName($sanitizedRequest['lastName']);
        $customer->setEmail($sanitizedRequest['email']);
        $customer->setCreatedAt($sanitizedRequest['createdAt']);
        $customer->setUpdatedAt(date('Y-m-d H:i:s'));
        
        try {
            if($customer->save() !== false) {
                $message = new Message(
                    Message::SUCCESS,
                    Message::RESOURCE_SUCCESSFULLY_UPDATED,
                    [Customers::RESOURCE_NAME, $customer->getLastName()]
                );
                $this->messageService->displayMessage($message, $this->flashSession);
            }
        } catch (\Exception $e) {
            // todo: log some data about the error and handle exceptions
            $message = new Message(
                Message::ERROR,
                Message::RESOURCE_NOT_SUCCESSFULLY_UPDATED,
                [Customers::RESOURCE_NAME, $customer->getLastName()]
            );
            $this->messageService->displayMessage($message, $this->flashSession);
            return (new Response())->redirect('/edit/' . $this->request->getPost('id'));
        }

        return (new Response())->redirect('/customers/list');
    }

    /**
     * delete an existing customer from the database
     */
    public function deleteAction($id)
    {
        if (!$id || !is_numeric($id)) {
            $message = new Message(
                Message::ERROR,
                Message::RESOURCE_ID_PRESENT_AND_NUMERIC_VALUE
                [Customers::RESOURCE_NAME]
            );
            $this->messageService->displayMessage($message, $this->flashSession);
            return (new Response())->redirect('/customers/');
        }
        $customer = Customers::findFirst($id);

        try {
            if ($customer->delete() !== false) {
                $message = new Message(
                    Message::SUCCESS,
                    Message::RESOURCE_SUCCESSFULLY_DELETED,
                    [Customers::RESOURCE_NAME, $customer->lastName]
                );
                $this->messageService->displayMessage($message, $this->flashSession);
            }
        } catch (\Exception $e) {
            // todo: log some data about the error and handle exceptions
            $message = new Message(
                Message::ERROR,
                Message::RESOURCE_NOT_SUCCESSFULLY_DELETED,
                [Customers::RESOURCE_NAME, $customer->lastName]
            );
            $this->messageService->displayMessage($message, $this->flashSession);
        }

        return (new Response())->redirect('/customers/list');
    }
}