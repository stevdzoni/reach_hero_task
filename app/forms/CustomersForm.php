<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Forms\Element\Submit;

class CustomersForm extends Form
{
    public function initialize($entity = null, $options = [])
    {
        $submitButtonText = 'Create';
        if (isset($options['edit']) && $options['edit']) {
            $this->add(new Hidden('id'));
            $submitButtonText = 'Update';
        }

        $firstName = new Text('firstName', ['class' => 'form-control']);
        $firstName->setLabel('First Name');
        $this->add($firstName);

        $lastName = new Text('lastName', ['class' => 'form-control']);
        $lastName->setLabel('Last Name');
        $this->add($lastName);

        $email = new Text('email', ['class' => 'form-control']);
        $email->setLabel('Email');
        $this->add($email);

        $this->add(new Submit($submitButtonText, ['class' => 'btn btn-primary btn-lg btn-block']));

        $this->add(new Hidden('createdAt'));
        $this->add(new Hidden('updatedAt'));

//        $csrf = new Hidden($this->security->getTokenKey());
//        $csrf->setDefault($this->security->getToken());
//        $this->add($csrf);
    }
}