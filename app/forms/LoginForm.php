<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;

class LoginForm extends Form
{
    public function initialize()
    {
        $userName = new Text('username', ['class' => 'form-control']);
        $userName->setLabel('Username');
        $this->add($userName);

        $password = new Password('password', ['class' => 'form-control']);
        $password->setLabel('Password');
        $this->add($password);

        $this->add(new Submit('Login', ['class' => 'btn btn-primary btn-lg btn-block']));

//        $csrf = new Hidden($this->security->getTokenKey());
//        $csrf->setDefault($this->security->getToken());
//        $this->add($csrf);
    }
}