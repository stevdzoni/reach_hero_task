<?php


class Message
{
    const ERROR = 'error';
    const SUCCESS = 'success';
    const NOTICE = 'notice';
    const WARNING = 'warning';

    const HTTP_METHOD_INVALID = 'httpMethodInvalid';
    const RESOURCE_SUCCESSFULLY_CREATED = 'resourceSuccessfullyCreated';
    const RESOURCE_NOT_SUCCESSFULLY_CREATED = 'resourceNotSuccessfullyCreated';
    const RESOURCE_SUCCESSFULLY_UPDATED = 'resourceSuccessfullyUpdated';
    const RESOURCE_NOT_SUCCESSFULLY_UPDATED = 'resourceNotSuccessfullyUpdated';
    const RESOURCE_ID_PRESENT_AND_NUMERIC_VALUE = 'resourceIdPresentAndNumericValue';
    const RESOURCE_SUCCESSFULLY_DELETED = 'resourceSuccessfullyDeleted';
    const RESOURCE_NOT_SUCCESSFULLY_DELETED = 'resourceNotSuccessfullyDeleted';

    const USERNAME_NOT_CORRECT = 'usernameNotCorrect';
    const PASSWORD_NOT_CORRECT = 'passwordNotCorrect';
    const USER_SESSION_EXPIRED = 'userSessionExpired';


    public string $type;
    public string $content;

    public function __construct($type, $key = null, $messageInput = [], $predefinedMessage = null)
    {
        $this->type = $type;
        $content = isset($key) ? $this->getMessageByKey($key) : $predefinedMessage;
        $this->content = count($messageInput) > 0 ? vsprintf($content, $messageInput) : $content;
    }

    public function getMessages()
    {
        $messagesJson = file_get_contents(APP_PATH . '/common/messages.json');
        return $messagesJson ? json_decode($messagesJson, true) : [];
    }

    public function getMessageByKey(string $key)
    {
        $resultMessage = false;

        $messages = $this->getMessages();
        if (! array_key_exists($key, $messages)) {
            return false;
        }
        foreach ($messages as $messageKey => $message) {
            if ($key == $messageKey) {
                $resultMessage = $message;
                break;
            }
        }

        return $resultMessage;
    }
}