<?php

use Phalcon\Mvc\Model;

class CustomersList extends Model
{
    private array $customers = [];

    public function addCustomerToList(Customers $customer)
    {
        $this->customers[] = $customer;
    }

    public function getAllCustomers()
    {
        return $this->customers;
    }

    public function getCount()
    {
        return count($this->customers) > 0 ? count($this->customers) : 0;
    }
}