<br>
<div class="flash-message-container">
    {% if flashSession.output() %}
        {{ flashSession.output() }}
    {% endif %}
</div>
{{ form('/customers/save') }}
    <h2>Edit an existing customer</h2>
    {% for element in form %}
        <div class="form-group">
            <div class="form-label-container">{{ element.getLabel() }}</div>
            {{ element }}
        </div>
    {% endfor %}
{{ endForm() }}
<br>
{% if flashSession.output() %}
    {{ flashSession.output() }}
{% endif %}