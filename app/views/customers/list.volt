<br>
<div class="flash-message-container">
    {% if flashSession.output() %}
        {{ flashSession.output() }}
    {% endif %}
</div>
<table class="table table-striped table-bordered table-hover resource-table">
    <thead>
    <tr>
        <td>#</td>
        <td>First name</td>
        <td>Last name</td>
        <td>Email</td>
        <td>Actions</td>
    </tr>
    </thead>
    <tbody>
    {% for customer in page.getItems() %}
        <tr>
            <td>{{ loop.index }}</td>
            <td>{{ customer['firstName'] }}</td>
            <td>{{ customer['lastName'] }}</td>
            <td>{{ customer['email'] }}</td>
            <td>
                <span class="btn btn-dark actions-link">{{ link_to('/customers/edit/' ~ customer['id'], 'Edit') }}</span>
                <span class="btn btn-danger actions-link">{{ link_to('/customers/delete/' ~ customer['id'], 'Delete') }}</span>
            </td>
        </tr>
    {% endfor %}
    </tbody>
</table>
<span class="page-numbers">Page {{ page.getCurrent() }} of {{ page.getLast() }}</span>
<div class="float-right">
    <nav>
        <ul class="pagination">
            <li class="page-link">{{ link_to('customers/list?page=' ~ page.getPrevious(), '<span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>', ['class':'page-link', 'aria-label':'Previous']) }}</li>
            <li class="page-link">{{ link_to('customers/list?page=' ~ page.getNext(), '<span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>', ['class':'page-link', 'aria-label':'Next']) }}</li>
        </ul>
    </nav>
</div>
<div class="clear"></div>
<div class="float-left navigation-buttons">
    <span class="btn btn-dark actions-link">{{ link_to('/welcome', 'Go to home page') }}</span>
</div>
<div class="float-right navigation-buttons">
    <span class="btn btn-dark actions-link">{{ link_to('/customers/new', 'Create a new customer') }}</span>
</div>