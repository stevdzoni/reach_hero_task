<br>
<div class="flash-message-container">
    {% if flashSession.output() %}
        {{ flashSession.output() }}
    {% endif %}
</div>
{{ form('/customers/create') }}
        <h2>Create a new customer</h2>
        <br>
        {% for element in form %}
            <div class="form-group">
                <div class="form-label-container">{{ element.getLabel() }}</div>
                {{ element }}
            </div>
        {% endfor %}
{{ endForm() }}
<br>

