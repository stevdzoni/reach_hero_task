<br>
<div class="text-center">';
<h1>Welcome, <i>{{ username }}!</i></h1>
<br>
    {{ link_to('customers/list', 'Customer list', 'class': 'btn btn-lg btn-primary') }}
    {{ link_to('customers/new', 'Create a new customer' , 'class': 'btn btn-lg btn-outline-primary') }}
</div>
<div>
    {{ link_to('/logout/logout', 'Logout' , 'class': 'btn btn-dark logout-button') }}
</div>