<br>
<div class="flash-message-container">
    {% if flashSession.output() %}
        {{ flashSession.output() }}
    {% endif %}
</div>
{{ form('/login') }}
{#        <input type="hidden" name="csrfToken" value="{{ form.getCsrfToken() }}">#}
<h2>Please login...</h2>
<br>
{% for element in form %}
    <div class="form-group">
        <div class="form-label-container">{{ element.getLabel() }}</div>
        {{ element }}
    </div>
{% endfor %}
{{ endForm() }}
<br>