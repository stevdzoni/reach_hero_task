<br>
<div class="flash-message-container">
    {% if flashSession.output() %}
        {{ flashSession.output() }}
    {% endif %}
</div>
<h2>Good bye!</h2>
<br>
<div><span class="btn btn-dark actions-link">{{ link_to('/', 'Go back to login page') }}</span></div>